const svg = document.querySelector('#svg');
const path = svg.querySelector('#main');
const lengthSlider = document.querySelector('#length-slider');
const widthSlider = document.querySelector('#width-slider');
const bendSlider = document.querySelector('#bend-slider');

const update = () => {
  path.setAttribute('d', `M 100 350 q ${lengthSlider.value / 2} ${bendSlider.value}  ${lengthSlider.value} 0`);
  path.setAttribute('stroke-width', widthSlider.value);
};

lengthSlider.addEventListener('input', update);
widthSlider.addEventListener('input', update);
bendSlider.addEventListener('input', update);
