export default class Quiz {
  constructor() {
    this.resize = this.resize.bind(this);

    this.element = document.createElement('iframe');
    this.element.className = 'quiz';
    this.element.setAttribute('src', 'assets/iframes/quiz/');
    this.element.setAttribute('scrolling', 'no');
    window.addEventListener('resize', this.resize);
  }

  show() {
    this.resize();
  }

  hide() {

  }

  resize() {
    const padding = 32;
    const width = 1080;
    const height = 1920;
    const { innerWidth, innerHeight } = window;
    const scale = (innerWidth - padding) / (innerHeight - padding) > width / height
      ? 1 / (height / (innerHeight - padding))
      : 1 / (width / (innerWidth - padding));
    this.element.style.transform = `scale(${scale}) translate(-50%, -50%)`;
  }
}
