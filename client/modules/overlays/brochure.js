export default class Brochure {
  constructor(state) {
    this.state = state;

    this.pageButtonClicked = this.pageButtonClicked.bind(this);

    this.element = document.createElement('div');
    this.element.className = 'brochure';

    this.pages = document.createElement('div');
    this.buttons = document.createElement('div');
    this.pages.className = 'brochure-pages';
    this.buttons.className = 'brochure-buttons';

    this.pageArray = [
      { file: 'assets/images/brochure-page-1.png', element: null, button: null },
      { file: 'assets/images/brochure-page-2.png', element: null, button: null },
      { file: 'assets/images/brochure-page-3.png', element: null, button: null },
      { file: 'assets/images/brochure-page-4.png', element: null, button: null },
    ];

    this.pageArray.forEach((page, index) => {
      page.element = document.createElement('div');
      page.element.className = `brochure-pages-page ${index === this.state.brochurePage ? '' : 'hidden'}`;
      page.element.style.backgroundImage = `url('${page.file}')`;
      this.pages.appendChild(page.element);

      page.button = document.createElement('button');
      page.button.className = `brochure-buttons-button ${index === this.state.brochurePage ? 'active' : ''}`;
      page.button.innerText = `Page ${index + 1}`;
      page.button.setAttribute('data-page', index);
      page.button.addEventListener('click', this.pageButtonClicked);
      this.buttons.appendChild(page.button);
    });

    this.download = document.createElement('button');
    this.download.innerText = 'Download Brochure PDF';
    this.download.className = 'brochure-download';
    this.download.addEventListener('click', () => { window.open('assets/pdfs/brochure.pdf'); });

    this.element.appendChild(this.pages);
    this.element.appendChild(this.buttons);
    this.element.appendChild(this.download);
  }


  show() {
    this.state.brochurePage = 0;
  }

  hide() {
  }

  gotoPage(previous, current) {
    if (previous !== null) this.pageArray[previous].element.classList.add('hidden');
    this.pageArray[current].element.classList.remove('hidden');

    if (previous !== null) this.pageArray[previous].button.classList.remove('active');
    this.pageArray[current].button.classList.add('active');
  }

  pageButtonClicked({ target }) {
    this.state.brochurePage = parseInt(target.getAttribute('data-page'), 10);
  }
}
