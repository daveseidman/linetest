export default class Intro {
  constructor(state) {
    this.state = state;

    this.element = document.createElement('div');
    this.element.className = 'intro';

    this.video = document.createElement('video');
    this.video.className = 'intro-video';
    this.video.src = 'assets/videos/msl-intro.mp4';
    this.video.setAttribute('controls', true);
    this.video.setAttribute('autoplay', true);

    this.startButton = document.createElement('button');
    this.startButton.className = 'intro-start';
    this.startButton.innerText = 'Enter';
    this.startButton.addEventListener('click', () => { this.state.overlay = null; });

    this.disclaimer = document.createElement('p');
    this.disclaimer.className = 'intro-disclaimer';
    this.disclaimer.innerText = 'Optimized for desktop viewing';

    this.element.appendChild(this.video);
    this.element.appendChild(this.startButton);
    this.element.appendChild(this.disclaimer);
  }

  show() {

  }

  hide() {

  }
}
