export default class Video {
  constructor(state) {
    this.state = state;

    this.element = document.createElement('div');
    this.element.className = 'video';

    this.player = document.createElement('video');
    this.player.className = 'video-player';
    this.player.src = 'assets/videos/noh-pathway.mp4';
    this.player.controls = true;

    this.element.appendChild(this.player);
  }

  show() {
    this.player.play();
  }

  hide() {
    this.player.pause();
  }
}
