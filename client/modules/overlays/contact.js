import { Email } from '../smtp';

export default class Contact {
  constructor(state) {
    this.state = state;

    this.submitForm = this.submitForm.bind(this);

    this.element = document.createElement('div');
    this.element.className = 'contact';

    this.form = document.createElement('div');
    this.form.className = 'contact-form';

    this.title = document.createElement('h1');
    this.title.className = 'contact-form-title';
    this.title.innerText = 'Send us a message:';

    this.name = document.createElement('input');
    this.name.className = 'contact-form-name';
    this.name.type = 'text';
    this.name.placeholder = 'Name';

    this.email = document.createElement('input');
    this.email.className = 'contact-form-email';
    this.email.type = 'email';
    this.email.placeholder = 'Email';

    this.message = document.createElement('textarea');
    this.message.className = 'contact-form-message';
    this.message.setAttribute('rows', 5);
    this.message.placeholder = 'Message';

    this.sendCopy = document.createElement('input');
    this.sendCopy.className = 'contact-form-sendcopy';
    this.sendCopy.type = 'checkbox';

    this.submit = document.createElement('button');
    this.submit.className = 'contact-form-send';
    this.submit.innerText = 'Submit';
    this.submit.addEventListener('click', this.submitForm);

    this.form.appendChild(this.title);
    this.form.appendChild(this.name);
    this.form.appendChild(this.email);
    this.form.appendChild(this.message);
    this.form.appendChild(this.sendCopy);
    this.form.appendChild(this.submit);

    this.complete = document.createElement('div');
    this.complete.className = 'contact-complete hidden';

    this.title = document.createElement('h1');
    this.title.className = 'contact-complete-title';
    this.title.innerText = 'Thanks for contacting us';

    this.text = document.createElement('p');
    this.text.className = 'contact-complete-text';
    this.text.innerText = 'One of our representatives will reply to your message within 5 business days.';

    this.close = document.createElement('button');
    this.close.className = 'contact-complete-close';
    this.close.innerText = 'Close';
    this.close.addEventListener('click', () => { this.state.overlay = null; });

    this.complete.appendChild(this.title);
    this.complete.appendChild(this.text);
    this.complete.appendChild(this.close);


    this.element.appendChild(this.form);
    this.element.appendChild(this.complete);
  }

  show() {
    this.form.classList.remove('hidden');
    this.complete.classList.add('hidden');
    this.name.value = '';
    this.email.value = '';
    this.message.value = '';
  }

  hide() {

  }

  submitForm() {
    this.form.classList.add('hidden');
    this.complete.classList.remove('hidden');
    console.log(this.message.value);
    console.log(this.name.value);
    console.log(this.email.value);
    Email.UseDefaultCredentials = false;
    Email.send({
      SecureToken: '387c6b79-73d7-45a1-ae8f-96ea1c865883',
      To: 'daveseidman@gmail.com',
      From: 'daveseidmanmechanism@gmail.com',
      Subject: `Message from Noh-Booth on behalf of ${this.name.value} <${this.email.value}>`,
      Body: this.message.value,
    }).then(
      message => console.log(message),
    );
  }
}
