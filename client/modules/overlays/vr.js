import { Scene, Raycaster, PerspectiveCamera, LinearFilter, WebGLRenderer, RGBFormat, Vector2, Vector3, DoubleSide, VideoTexture, PlaneBufferGeometry, SphereBufferGeometry, BoxGeometry, TextureLoader, MeshBasicMaterial, Mesh, Object3D } from 'three';
import OrbitControls from 'three-orbitcontrols';
import { autoPlay, Tween, Easing } from 'es6-tween';
import { DegToRad, RadToDeg } from '../utils';

const zoom = {
  min: 0.5,
  max: 2,
};
const padding = 32;
const resetCameraThreshold = 20; // if camera is more than n degrees off center left or right
const resetCameraTimeoutDuration = 1; // reset it after n seconds
const resetCameraSpeed = 3; // over this amount of time

export default class VR {
  constructor() {
    this.zoom = this.zoom.bind(this);
    this.render = this.render.bind(this);
    this.resize = this.resize.bind(this);
    this.mouseDown = this.mouseDown.bind(this);
    this.mouseMove = this.mouseMove.bind(this);
    this.mouseUp = this.mouseUp.bind(this);
    this.resetCamera = this.resetCamera.bind(this);
    this.show = this.show.bind(this);
    this.hide = this.hide.bind(this);

    autoPlay(true);

    this.scene = new Scene();
    this.camera = new PerspectiveCamera(75, 1, 0.1, 200);
    this.camera.startPosition = new Vector3(1, 0, -0.5);
    this.camera.position.set(this.camera.startPosition.x, this.camera.startPosition.y, this.camera.startPosition.z); // orbit controls don't work when camera is at origin
    this.camera.previousPosition = this.camera.position;
    this.camera.rotation.order = 'YXZ';
    this.renderer = new WebGLRenderer({ antialias: false });
    this.renderer.setPixelRatio(1);
    this.element = document.createElement('div');
    this.element.className = 'vr';

    this.resume = document.createElement('button');
    this.resume.className = 'vr-resume';
    this.resume.innerText = 'Click here to resume';
    this.resume.addEventListener('click', this.show);

    this.element.appendChild(this.renderer.domElement);
    this.element.appendChild(this.resume);

    const geometry = new SphereBufferGeometry(100, 50, 100);
    geometry.scale(-1, 1, 1);

    this.video = document.createElement('video');
    this.video.src = 'assets/videos/panorama.mp4';

    const texture = new VideoTexture(this.video);
    texture.minFilter = LinearFilter;
    texture.magFilter = LinearFilter;
    texture.format = RGBFormat;
    const material = new MeshBasicMaterial({ color: 0xffffff, map: texture, side: DoubleSide });

    this.panorama = new Mesh(geometry, material);
    this.scene.add(this.panorama);

    this.controls = new OrbitControls(this.camera, this.element);
    this.controls.enableDamping = true;
    this.controls.dampingFactor = 0.05;
    this.controls.enablePan = false;
    this.controls.rotateSpeed = -0.25;
    this.controls.enableZoom = false;
    this.controls.target = new Vector3(0, 0, 0);
    this.controls.minPolarAngle = DegToRad(50);
    this.controls.maxPolarAngle = DegToRad(130);
    this.controls.update();
    this.camera.startAngle = this.camera.rotation.y;

    this.timeout = null;
    this.mouse = new Vector2();
    this.raycaster = new Raycaster();
    this.hover = null;

    this.resize();

    window.addEventListener('resize', this.resize);
    window.addEventListener('mousemove', this.mouseMove);
    window.addEventListener('mousedown', this.mouseDown);
    window.addEventListener('mouseup', this.mouseUp);
    // window.addEventListener('wheel', this.zoom);
    // this.element.addEventListener('click', this.mouseClick);
  }

  show() {
    this.visible = true;
    // TODO: make this DRY:
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    if (this.tween) {
      this.tween.pause();
      this.tween = null;
    }
    this.controls.enableDamping = true;
    this.video.currentTime = 0;
    this.zoomTarget = 1;
    this.camera.zoom = 1;
    this.camera.updateProjectionMatrix();
    this.camera.position.set(this.camera.startPosition.x, this.camera.startPosition.y, this.camera.startPosition.z); // orbit controls don't work when camera is at origin


    async function playVideo(_this) {
      try {
        await _this.video.play();
        _this.render();
        _this.resume.classList.add('hidden');
      } catch (e) {
        _this.resume.classList.remove('hidden');
        console.log('show resume button');
      }
    }
    playVideo(this);
  }

  hide() {
    this.visible = false;
    this.video.pause();
    this.controls.enableDamping = false;
  }

  resetCamera() {
    if (Math.abs(RadToDeg(this.camera.rotation.y) - RadToDeg(this.camera.startAngle)) > resetCameraThreshold) {
      this.tween = new Tween(this.camera)
        .to({ zoom: 1, position: this.camera.startPosition }, resetCameraSpeed * 1000)
        .easing(Easing.Quintic.InOut)
        .on('update', ({ zoom }) => {
          this.camera.updateProjectionMatrix(); this.zoomTarget = zoom;
        })
        .start();
    }
  }

  mouseDown() {
    this.element.classList.add('grabbing');
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
    if (this.tween) {
      this.tween.pause();
      this.tween = null;
    }
  }

  mouseMove({ clientX, clientY }) {

  }

  mouseUp() {
    this.element.classList.remove('grabbing');
    this.timeout = setTimeout(this.resetCamera, resetCameraTimeoutDuration * 1000);
  }

  zoom({ deltaY }) {
    this.zoomTarget += deltaY / -1000;
    this.zoomTarget = Math.max(Math.min(this.zoomTarget, zoom.max), zoom.min);
    // if (this.timeout) clearTimeout(this.timeout);
    // this.timeout = setTimeout(this.resetCamera, resetCameraTimeoutDuration * 1000);

    // this.camera.updateProjectionMatrix();
  }

  render() {
    // zoom in out eased
    if (!this.tween && Math.abs(this.camera.zoom - this.zoomTarget) > 0.001) {
      this.camera.zoom += (this.zoomTarget - this.camera.zoom) / 10;
      this.camera.updateProjectionMatrix();
    }

    this.controls.update();

    this.camera.previousPosition = this.camera.position.clone();
    this.renderer.render(this.scene, this.camera);
    if (this.visible) requestAnimationFrame(this.render);
  }

  resize() {
    this.camera.aspect = (window.innerWidth - padding) / (window.innerHeight - padding);
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(window.innerWidth - padding, window.innerHeight - padding);
  }
}
