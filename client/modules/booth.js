export default class Booth {
  constructor(state) {
    this.state = state;

    this.buttonClicked = this.buttonClicked.bind(this);

    this.element = document.createElement('div');
    this.element.className = 'booth';

    this.background = document.createElement('img');
    this.brochureButton = document.createElement('img');
    this.quizButton = document.createElement('img');
    this.videoButton = document.createElement('img');
    this.vrButton = document.createElement('img');
    this.contactButton = document.createElement('button');

    this.background.className = 'booth-background';
    this.brochureButton.className = 'booth-button brochure';
    this.quizButton.className = 'booth-button quiz';
    this.videoButton.className = 'booth-button video';
    this.vrButton.className = 'booth-button vr';
    this.contactButton.className = 'booth-button contact';

    this.brochureButton.setAttribute('data-overlay', 'brochure');
    this.quizButton.setAttribute('data-overlay', 'quiz');
    this.videoButton.setAttribute('data-overlay', 'video');
    this.vrButton.setAttribute('data-overlay', 'vr');
    this.contactButton.setAttribute('data-overlay', 'contact');

    this.background.src = 'assets/images/background.png';
    this.brochureButton.src = 'assets/images/brochure-button.png';
    this.quizButton.src = 'assets/images/quiz-button.png';
    this.videoButton.src = 'assets/images/video-button.png';
    this.vrButton.src = 'assets/images/vr-button.png';
    this.contactButton.innerText = 'CONTACT';

    this.brochureButton.addEventListener('click', this.buttonClicked);
    this.quizButton.addEventListener('click', this.buttonClicked);
    this.videoButton.addEventListener('click', this.buttonClicked);
    this.vrButton.addEventListener('click', this.buttonClicked);
    this.contactButton.addEventListener('click', this.buttonClicked);

    this.element.appendChild(this.background);
    this.element.appendChild(this.brochureButton);
    this.element.appendChild(this.quizButton);
    this.element.appendChild(this.videoButton);
    this.element.appendChild(this.vrButton);
    this.element.appendChild(this.contactButton);
  }

  buttonClicked({ target }) {
    this.state.overlay = target.getAttribute('data-overlay');
  }
}
