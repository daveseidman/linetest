export default class JobCode {
  constructor(state) {
    this.state = state;

    this.element = document.createElement('div');
    this.element.className = 'jobcode';

    this.element.innerText = '©2020 Lundbeck. All rights researved. UBR-0-XXXXXX.';
  }
}
