import Intro from './overlays/intro';
import Video from './overlays/video';
import Brochure from './overlays/brochure';
import VR from './overlays/vr';
import Quiz from './overlays/quiz';
import Contact from './overlays/contact';

export default class Overlays {
  constructor(state) {
    this.state = state;

    this.element = document.createElement('div');
    this.element.className = 'overlays hidden';

    this.content = document.createElement('div');
    this.content.className = 'overlays-content';

    this.closeButton = document.createElement('button');
    this.closeButton.className = 'overlays-close';
    this.closeButton.innerText = 'x';
    this.closeButton.addEventListener('click', () => { this.state.overlay = null; });
    this.element.addEventListener('click', ({ target }) => {
      console.log(target.parentNode);
      // if (target.parentNode.classList.contains('overlays-content')) this.state.overlay = null;
    });
    this.element.appendChild(this.content);
    this.element.appendChild(this.closeButton);

    this.intro = new Intro(this.state);
    this.video = new Video(this.state);
    this.brochure = new Brochure(this.state);
    this.vr = new VR(this.state);
    this.quiz = new Quiz(this.state);
    this.contact = new Contact(this.state);
  }

  show(overlay) {
    this.element.classList.remove('hidden');
    this.content.innerHTML = '';

    this.content.appendChild(this[overlay].element);
    this[overlay].show();
    this.closeButton.classList[overlay === 'intro' ? 'add' : 'remove']('hidden');
  }

  hide(overlay) {
    this[overlay].hide();
    this.element.classList.add('hidden');
  }
}
