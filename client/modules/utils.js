module.exports.DegToRad = deg => deg * Math.PI / 180;
module.exports.RadToDeg = rad => rad * 180 / Math.PI;
