export default class Title {
  constructor(state) {
    this.state = state;

    this.element = document.createElement('div');
    this.element.className = 'title';

    this.main = document.createElement('div');
    this.main.className = 'title-main';
    this.main.addEventListener('click', () => { this.state.overlay = 'intro'; });

    this.logo = document.createElement('img');
    this.logo.className = 'title-main-logo';
    this.logo.src = 'assets/images/lundbeck-logo.png';


    this.mainText = document.createElement('h1');
    this.mainText.className = 'title-main-text';
    this.mainText.innerText = 'EXPLOE nOH';

    this.main.appendChild(this.logo);
    this.main.appendChild(this.mainText);

    this.text = document.createElement('h2');
    this.text.className = 'title-text';
    this.text.innerText = 'A virtual space to experience the many sides of neurogenic orthostatic hypotension (nOH)';

    this.element.appendChild(this.main);
    this.element.appendChild(this.text);
  }
}
